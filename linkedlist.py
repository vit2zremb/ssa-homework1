"""
Create LinkedList Class with methods:
 a) get(i)
 b) put(i, val)
 c) delete(i)
 d) index_of(el) - first element = el
 e) size()

 Cover your LinkedList class methods with unit tests. (Possible to use setUp() while testing)
"""


class LinkedList:
    def __init__(self, init_list=()):
        self.first = self.Node(None)
        self.last = self.Node(None, prev_node=self.first)
        self.last.prev = self.first
        self.first.next = self.last
        self._length = 0
        for val in init_list:
            self.put(len(self), val)

    def put(self, index, value):
        self._check_index(index, on_insert=True)
        it = self.first
        for i in range(index):
            it = it.next
        self._insert_between(value, prev_node=it, next_node=it.next)

    def get(self, index):
        self._check_index(index)
        it = self.first
        for i in range(index):
            it = it.next
        return it.next.value

    def delete(self, index):
        self._check_index(index)
        if index == 0 and len(self) == 0:
            raise IndexError("Nothing to delete")
        it = self.first
        for i in range(index + 1):
            it = it.next
        self._delete_before(it.next)

    def index_of(self, element):
        it = self.first
        i = 0
        while it.next != self.last:
            it = it.next
            if it.value == element:
                return i
            i += 1
        raise ValueError("Element {} is not in list".format(element))

    def size(self):
        return self._length

    def __len__(self):
        return self.size()

    def _check_index(self, index, on_insert=False):
        if (index < 0) or (index > len(self) - (0 if on_insert else 1)):
            raise IndexError("LinkedList index out of range.")

    def _insert_between(self, value, prev_node, next_node):
        new_node = self.Node(value, prev_node=prev_node, next_node=next_node)
        prev_node.next = new_node
        next_node.prev = new_node
        self._length += 1

    def _delete_before(self, node):
        node_to_delete = node.prev
        prev = node_to_delete.prev
        prev.next = node
        node.prev = prev
        self._length -= 1

    class Node:
        def __init__(self, value, prev_node=None, next_node=None):
            self.prev = prev_node
            self.next = next_node
            self.value = value
