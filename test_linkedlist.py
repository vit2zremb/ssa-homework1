from linkedlist import LinkedList
import unittest


class TestLinkedList(unittest.TestCase):
    def setUp(self):
        self.linked_list = LinkedList()

    def test_len_of_empty_list(self):
        self.assertEqual(len(self.linked_list), 0, "Length of empty list should be 0")

    def test_len_after_put_in(self):
        self.linked_list.put(0, 40)
        self.assertEqual(len(self.linked_list), 1)
        self.linked_list.put(0, 100)
        self.assertEqual(len(self.linked_list), 2)
        self.assertEqual(len(self.linked_list), self.linked_list.size())

    def test_get_on_empty(self):
        with self.assertRaises(IndexError):
            self.linked_list.get(0)

    def test_delete_on_empty(self):
        with self.assertRaises(IndexError):
            self.linked_list.delete(0)

    def test_put_and_get_methods(self):
        self.linked_list.put(0, 111)
        self.assertEqual(self.linked_list.get(0), 111)
        self.linked_list.put(0, 222)
        self.assertEqual(self.linked_list.get(0), 222)
        self.linked_list.put(1, 123)
        self.assertEqual(self.linked_list.get(1), 123)
        self.linked_list.put(0, 1)
        self.assertEqual(self.linked_list.get(0), 1)


class TestNonEmptyLinkedList(unittest.TestCase):
    def setUp(self):
        self.linked_list = LinkedList(['1', '2', '3'])

    def test_len(self):
        self.assertEqual(len(self.linked_list), 3)

    def test_index_of(self):
        self.assertEqual(self.linked_list.index_of('1'), 0)
        self.assertEqual(self.linked_list.index_of('2'), 1)
        self.assertEqual(self.linked_list.index_of('3'), 2)
        with self.assertRaises(ValueError):
            self.linked_list.index_of('666')

    def test_len_after_delete_first(self):
        self.linked_list.delete(0)
        self.assertEqual(len(self.linked_list), 2)

    def test_len_after_delete_last(self):
        self.linked_list.delete(2)
        self.assertEqual(len(self.linked_list), 2)

    def test_len_after_delete_middle(self):
        self.linked_list.delete(1)
        self.assertEqual(len(self.linked_list), 2)

    def test_delete_first(self):
        self.linked_list.delete(0)
        with self.assertRaises(ValueError):
            self.linked_list.index_of('1')
        self.assertEqual(self.linked_list.index_of('2'), 0)

    def test_delete_last(self):
        self.linked_list.delete(2)
        with self.assertRaises(ValueError):
            self.linked_list.index_of('3')

    def test_delete_middle(self):
        self.linked_list.delete(1)
        with self.assertRaises(ValueError):
            self.linked_list.index_of('2')
        self.assertEqual(self.linked_list.index_of('3'), 1)

    def test_put_first(self):
        self.linked_list.put(0, '777')
        self.assertEqual(self.linked_list.index_of('777'), 0)
        self.assertEqual(len(self.linked_list), 4)
        self.assertEqual(self.linked_list.index_of('1'), 1)

    def test_put_last(self):
        self.linked_list.put(3, '777')
        self.assertEqual(self.linked_list.index_of('777'), 3)
        self.assertEqual(len(self.linked_list), 4)
        self.assertEqual(self.linked_list.index_of('3'), 2)

    def test_put(self):
        self.linked_list.put(1, '777')
        self.assertEqual(self.linked_list.index_of('777'), 1)
        self.assertEqual(len(self.linked_list), 4)
        self.assertEqual(self.linked_list.index_of('2'), 2)

    def test_get(self):
        self.assertEqual(self.linked_list.get(0), '1')
        self.assertEqual(self.linked_list.get(1), '2')
        self.assertEqual(self.linked_list.get(2), '3')
        with self.assertRaises(IndexError):
            self.linked_list.get(3)


if __name__ == "__main__":
    unittest.main()
