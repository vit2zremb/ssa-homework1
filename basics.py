"""
1. Define a function, that takes string as argument and prints "Hello, %arg%!"
2. Define a function sum() and a function multiply() that sums and multiplies (respectively) all the numbers in a list
   of numbers. For example, sum([1, 2, 3, 4]) should return 10, and multiply([1, 2, 3, 4]) should return 24.
3. Define a function reverse() that computes the reversal of a string. For example, reverse("I am testing") should
   return the string "gnitset ma I".
4. Define a function is_palindrome() that recognizes palindromes (i.e. words that look the same written backwards).
   For example, is_palindrome("radar") should return True.
5. Define a function histogram() that takes a list of integers and prints a histogram to the screen. For example,
   histogram([4, 9, 7]) should print the following:
****
*********
******
(usage time.sleep(s) possible for better visualization)
6. Define a function caesar_cipher that takes string and key(number), which returns encrypted string
7. define a function diagonal_reverse() that takes matrix and returns diagonal-reversed one:
1 2 3         1 4 7
4 5 6 returns 2 5 8
7 8 9         3 6 9

8. Write a function game() number-guessing game, that takes 2 int parameters defining the range.
   Using random.randint(A, B) generate random int from the range. While user input isn't equal that number,
   print "Try again!". If user guess the number, congratulate him and exit. (use raw_input())
9. Define a function, which takes a string with N opening brackets ("[") and N closing brackets ("]"),
   in some arbitrary order.
    Determine whether the generated string is balanced; that is, whether it consists entirely of pairs of opening/closing
    brackets (in that order), none of which mis-nest.
    Examples:
       []        OK   ][        NOT OK
       [][]      OK   ][][      NOT OK
       [[][]]    OK   []][[]    NOT OK
10. Write a function char_freq() that takes a string and builds a frequency listing of the characters contained in it.
    Represent the frequency listing as a Python dictionary.
    Try it with something like char_freq("abbabcbdbabdbdbabababcbcbab").
11. Write a function dec_to_bin() that takes decimal integer and outputs its binary representation.
12. Write a ship battle game, which is similar to ex.8, except it takes 1 integer as an order of matrix,
    randomly generates index (x, y) and checks user input (2 integers).
(tip: use var1, var2 = raw_input("Enter two numbers here: ").split())
*Visualize the game.
"""

from string import ascii_letters
from copy import deepcopy
import random
from functools import reduce


# 1
def task1(arg):
    print('Hello {}!'.format(arg))


# 2
# function sum is built in
def sum1(lst):
    return sum(lst)


def sum2(lst):
    res = 0
    for val in lst:
        res += val
    return res


def sum3(lst):
    return reduce(lambda acc, val: acc + val, lst, 0)


def multiply(lst):
    res = 1
    for val in lst:
        res *= val
    return res


def multiply2(lst):
    return reduce(lambda acc, val: acc * val, lst, 1)


# 3
def reverse(s):
    return ''.join(reversed(s))


# 4
def is_palindrome(s):
    return s == reverse(s)


# 5
def histogram(data):
    for val in data:
        print('*' * val)


# 6
def caesar_cipher(s, key):
    """
    This cipher works only with ascii letters (both lower and upper case), other characters remains unchanged
    """
    key = key % len(ascii_letters)  # normalise key
    trans = str.maketrans(ascii_letters, ascii_letters[key:] + ascii_letters[:key])
    return s.translate(trans)


# 7
def diagonal_reverse(matrix):
    height = len(matrix)
    width = len(matrix[0])
    res_matrix = deepcopy(matrix)
    for row in range(height):
        for col in range(width):
            res_matrix[col][row] = matrix[row][col]
    return res_matrix


# 8
def game(begin, end):
    num = random.randint(begin, end)
    print("Guess the number between {} and {} (both inclusive): ".format(begin, end))
    while True:
        guess = input()
        if num == int(guess):
            break
        print("Try again:")
    print("You won!")


# 9
def is_balanced(s):
    open_braces = 0
    for char in s:
        if char == '[':
            open_braces += 1
        if char == ']':
            open_braces -= 1
        if open_braces < 0:
            return False
    return open_braces == 0


# 10
def char_freq(s):
    d = dict()
    for char in set(s):
        d[char] = s.count(char)
    return d


# 11
def dec_to_bin(num):
    return str(bin(num))[2:]


# 12
def battle_ship_game(n):
    target_x = random.randint(0, n - 1)
    target_y = random.randint(0, n - 1)
    battlefield = {(target_x, target_y): 2}

    def print_battlefield():
        print('Try again:')
        res = ""
        for x in range(n):
            for y in range(n):
                val = battlefield.get((x, y), 0)
                if val == 1:
                    res += '* '
                elif val == 3:
                    res += 'X '
                else:
                    res += '. '
            res += '\n'
        print(res)

    print("Guess the pair of numbers in range [{}, {}]: ".format(0, n - 1))
    while True:
        print_battlefield()
        x, y = input().split()
        x, y = int(x), int(y)
        if (x, y) == (target_x, target_y):
            battlefield[x, y] = 3
            break
        else:
            battlefield[x, y] = 1
    print_battlefield()
    print("You won!")


if __name__ == "__main__":
    print(1)
    task1("there")
    print()

    print(2)
    arg = [1, 2, 3, 4]
    print("arg =", arg)
    print("sum1(arg) =", (sum1(arg)))
    print("sum2(arg) =", (sum2(arg)))
    print("sum3(arg) =", (sum3(arg)))
    print("multiply(arg) =", (multiply(arg)))
    print("multiply2(arg) =", (multiply2(arg)))
    print()

    print(3)
    print("reverse('hello') =", (reverse('hello')))
    print()

    print(4)
    print("is_palindrome('12321') =", (is_palindrome('12321')))
    print("is_palindrome('1232') =", (is_palindrome('1232')))
    print()

    print(5)
    print("Histogram [4, 10, 7, 2]")
    histogram([4, 10, 7, 2])
    print()

    print(6)
    print("caesar_cipher('abcd', 1) =", caesar_cipher('abcd', 1))
    print("caesar_cipher('abcd', -1) =", caesar_cipher('abcd', -1))
    print('caesar_cipher("Hello, my name is John Cena.", 222) =', caesar_cipher("Hello, my name is John Cena.", 222))
    print('caesar_cipher("VszzC, AM BoAs wG XCvB QsBo.", -222) =', caesar_cipher("VszzC, AM BoAs wG XCvB QsBo.", -222))
    print()

    print(7)
    print("diagonal_reverse([[1, 2, 3], [4, 5, 6], [7, 8, 9]]) =",
          diagonal_reverse([[1, 2, 3], [4, 5, 6], [7, 8, 9]]))
    print()

    print(8)
    game(0, 1)
    print()

    print(9)
    print("[[][]]", is_balanced('[[][]]'))
    print("[]][[]", is_balanced('[]][[]'))
    print("][][", is_balanced('][]['))
    print()

    print(10)
    print("char_freq('abbabcbdbabdbdbabababcbcbab') =", char_freq('abbabcbdbabdbdbabababcbcbab'))
    print("char_freq('asdfkas;dlfkj') =", char_freq('asdfkas;dlfkj'))
    print()

    print(11)
    print("dec_to_bin(15) =", dec_to_bin(15))
    print("dec_to_bin(17) =", dec_to_bin(17))
    print("dec_to_bin(2) =", dec_to_bin(2))
    print()

    print(12)
    battle_ship_game(2)
    print()
