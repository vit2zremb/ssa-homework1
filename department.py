"""
1. Create structure for department:
 a) + There are 3 types of employee: developer, designer and manager
 b) + Each employee has: first name, second name, salary, experience (years) and manager
 c) + Each designer has effectiveness coefficient(0-1)
 d) + Each manager has team of developers and designers.
 e) + Department should have list of managers(which have their own teams)
 f) Department should be able to give salary (for each employee message: 
    "@firstName@ @secondName@: got salary: @salaryValue@")
 g) + Each employee gets the salary, defined in field Salary. If experience of employee is > 2 years,
    he gets bonus + 200$, if experience is > 5 years, he gets salary*1.2 + bonus 500
 h) + Each designer gets the salary = salary * eff_coef
 i) + Each manager gets salary
   ii) 200$ if his team has >5 members
   iii) 300$ if his team has >10 members
   iiii) salary*1.1 if more than half of team members are developers.
 j) + Redefine string representation for employee as follows:
    "@firstName@ @secondName@, manager:@manager secondName@, experience:@experience@"

    1. Modify your department:
 a) + change giving salary method: if there is not Employee in manager's team, raise SalaryGivingError
    (You should create the Error class) for manager.
 b) + Add method add_to_team(array) to manager class, which adds employees to team. If there are not Employees in array,
    raise your own NotEmployeeException and if Employee is Manager,
    raise WrongEmployeeRoleError with parameter "second_name".
    String representation of error should return message "Employee @second_name@ has unexpected role!"
 c) + In Department class create method add_team_members(manager, array) which tries to add array of Employees to manager
    team. Handle the exceptions, raised in manager's add_to_team method.
"""


class Employee:
    def __init__(self, first_name: str, second_name: str, salary=0):
        self._first_name = first_name
        self._second_name = second_name
        self._salary = salary
        self._experience = 0
        self.manager = None

    def __str__(self):
        if isinstance(self.manager, Manager):
            return "{first_name} {second_name}, manager:{manager_second_name}, experience:{experience}".format(
                first_name=self.first_name,
                second_name=self.second_name,
                manager_second_name=self.manager.second_name,
                experience=self.experience
            )
        else:
            return "{}: {} {}".format(type(self), self._first_name, self._second_name)

    @property
    def first_name(self):
        return self._first_name

    @property
    def second_name(self):
        return self._second_name

    @property
    def salary(self):
        """
        Each employee gets the salary, defined in field Salary. If experience of employee is > 2 years,
        he gets bonus + 200$, if experience is > 5 years, he gets salary*1.2 + bonus 500
        """
        if self.experience > 2:
            return self._salary + 200
        elif self.experience > 5:
            return self._salary * 1.2 + 500
        else:
            return self._salary

    @salary.setter
    def salary(self, value):
        self._salary = value

    @property
    def experience(self):
        return self._experience

    @experience.setter
    def experience(self, value):
        self._experience = value


class Developer(Employee):
    pass


class Designer(Employee):
    def __init__(self, first_name, second_name, salary=0, eff_coef=0.0):
        super().__init__(first_name, second_name)
        self._eff_coef = 0

    @property
    def effectiveness_coefficient(self):
        return self._eff_coef

    @effectiveness_coefficient.setter
    def effectiveness_coefficient(self, value: float):
        if 0 <= value <= 1:
            self._eff_coef = value
        else:
            raise InvalidCoefficientError("An effectiveness coefficient cannot be outside the range [0-1]", value)

    @property
    def salary(self):
        return super().salary * self.effectiveness_coefficient


class Manager(Employee):
    def __init__(self, first_name, second_name, salary=0):
        super().__init__(first_name, second_name, salary=salary)
        self._team = set()

    def add_employee(self, employee):
        if isinstance(employee, (Designer, Developer)):
            self._team.add(employee)
        else:
            raise WrongEmployeeRoleError(employee.second_name)

    def add_to_team(self, array):
        if not array:
            raise NotEmployeeException("There are no employees in passed array")
        for member in array:
            self.add_employee(member)

    @property
    def salary(self):
        if len(self.team) > 10:
            bonus = 300
        elif len(self.team) > 5:
            bonus = 200
        else:
            bonus = 0
        salary = super().salary + bonus
        if self.developers_ratio() > 0.5:
            return salary * 1.1
        else:
            return salary

    def developers_ratio(self):
        return len(self.developers) / len(self.team)

    @property
    def developers(self):
        return [employee for employee in self.team if isinstance(employee, Developer)]

    @property
    def team(self):
        return self._team


class Department:
    """
    Department can give salary
    """

    def __init__(self, managers=()):
        self._managers = list(managers)

    def give_salary(self):
        for manager in self._managers:  # manager: Manager
            if not manager.team:
                raise SalaryGivingError("Manager {} does not have team members".format(manager.second_name))
            for team_member in manager.team:
                self._give_salary_for_employee(team_member)
            self._give_salary_for_employee(manager)

    def add_team_members(self, manager, array):
        if manager not in self._managers:
            self._managers.append(manager)
        try:
            manager.add_to_team(array)
        except WrongEmployeeRoleError as e:
            print("Cannot add one of the members to {}'s team".format(manager.second_name))
        except NotEmployeeException as e:
            print("Array without employees passed")

    @staticmethod
    def _give_salary_for_employee(employee: Employee):
        salary = employee.salary
        print("{} {}: got salary: {}".format(employee.first_name, employee.second_name, salary))
        return salary


class SalaryGivingError(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(*args, **kwargs)


class WrongEmployeeRoleError(Exception):
    def __init__(self, second_name, *args, **kwargs):
        Exception.__init__(*args, **kwargs)
        self._second_name = second_name

    def __str__(self):
        return "Employee {} has unexpected role!".format(self._second_name)


class NotEmployeeException(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(*args, **kwargs)


class InvalidCoefficientError(Exception):
    def __init__(self, message, val):
        self.value = val

    def __str__(self):
        return "InvalidCoefficientException: coefficient = {}".format(self.value)


if __name__ == '__main__':
    department = Department()
    department.add_team_members(Manager('Petro', 'Sydorenko', 500), [
        Designer('Van', 'Helsing', 1000, eff_coef=0.6), Developer('Hackerman', 'Hatsker', 2000),
        Developer('Haskell', 'Master2', 3000), Designer('Volodymyr', 'Morshyn', 4000),
        Developer('Hello', 'World', 5000)
    ])
    department.give_salary()
